<?php

/**
 * @file
 * Contains \Drupal\status_update\Form\BlockDeleteForm.
 */

namespace Drupal\status_update\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;

/**
 * Provides a deletion confirmation form for the status update instance deletion form.
 */
class StatusUpdateDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the status update %name?', array('%name' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelRoute() {
    return array(
      'route_name' => 'status_update.edit',
      'route_parameters' => array('status_update' => $this->entity->id()),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array $form, array &$form_state) {
    $this->entity->delete();
    watchdog('content', 'Status update %title deleted.', array('%title' => $this->entity->label()));
    drupal_set_message($this->t('The status update %name has been deleted.', array('%name' => $this->entity->label())));
    $form_state['redirect'] = '';
  }

}