<?php
/**
 * @file
 * Contains \Drupal\hello_world\Controller\HelloWorldController.
 */

namespace Drupal\rms\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * HelloWorldController.
 */
class RmsController extends ControllerBase {
  /**
   * Generates an example page.
   */
  public function content() {
    return array(
      '#type' => 'markup',
      '#markup' => t('Hello world'),
    );
  }
}
